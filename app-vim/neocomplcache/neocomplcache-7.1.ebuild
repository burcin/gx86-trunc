# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /mnt/disk/cgit/cvsroot/gentoo-x86/app-vim/neocomplcache/neocomplcache-7.1.ebuild,v 1.2 2014/02/24 00:52:51 phajdan.jr Exp $

EAPI=4

inherit vim-plugin

DESCRIPTION="vim plugin: ultimate auto completion system"
HOMEPAGE="http://www.vim.org/scripts/script.php?script_id=2620"
LICENSE="MIT"
KEYWORDS="~amd64 x86"
IUSE=""

VIM_PLUGIN_HELPFILES="${PN}.txt"
