# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /mnt/disk/cgit/cvsroot/gentoo-x86/dev-perl/Set-IntSpan/Set-IntSpan-1.190.0.ebuild,v 1.4 2014/03/02 10:31:53 pacho Exp $

EAPI=4

MODULE_AUTHOR=SWMCD
MODULE_VERSION=1.19
inherit perl-module

DESCRIPTION="Manages sets of integers"

SLOT="0"
KEYWORDS="~alpha amd64 hppa ~ppc x86 ~amd64-linux ~x86-linux"
IUSE=""

SRC_TEST="do"
