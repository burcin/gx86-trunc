# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /mnt/disk/cgit/cvsroot/gentoo-x86/dev-perl/Sys-SigAction/Sys-SigAction-0.200.0.ebuild,v 1.2 2014/02/24 02:02:20 phajdan.jr Exp $

EAPI=4

MODULE_AUTHOR=LBAXTER
MODULE_VERSION=0.20
inherit perl-module

DESCRIPTION="Perl extension for Consistent Signal Handling"

SLOT="0"
KEYWORDS="~amd64 ~arm ~ppc x86"
IUSE=""

SRC_TEST=do
