# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /mnt/disk/cgit/cvsroot/gentoo-x86/sec-policy/selinux-pcscd/selinux-pcscd-9999.ebuild,v 1.1 2014/02/17 20:58:41 swift Exp $
EAPI="4"

IUSE=""
MODS="pcdcd"
BASEPOL="9999"

inherit selinux-policy-2

DESCRIPTION="SELinux policy for pcdcd"

KEYWORDS=""
