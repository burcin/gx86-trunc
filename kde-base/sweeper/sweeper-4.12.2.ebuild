# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /mnt/disk/cgit/cvsroot/gentoo-x86/kde-base/sweeper/sweeper-4.12.2.ebuild,v 1.1 2014/02/06 23:20:26 dilfridge Exp $

EAPI=5

KDE_HANDBOOK="optional"
inherit kde4-base

DESCRIPTION="KDE Privacy Settings Widget"
HOMEPAGE="http://www.kde.org/applications/utilities/sweeper
http://utils.kde.org/projects/sweeper"
KEYWORDS=" ~amd64 ~arm ~ppc ~ppc64 ~x86 ~amd64-linux ~x86-linux"
IUSE="debug"
