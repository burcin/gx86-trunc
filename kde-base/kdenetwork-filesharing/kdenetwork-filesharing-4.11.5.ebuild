# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /mnt/disk/cgit/cvsroot/gentoo-x86/kde-base/kdenetwork-filesharing/kdenetwork-filesharing-4.11.5.ebuild,v 1.6 2014/02/23 09:08:37 ago Exp $

EAPI=5

inherit kde4-base

DESCRIPTION="kcontrol filesharing config module for SMB"
KEYWORDS="amd64 ~arm ppc ppc64 x86 ~amd64-linux ~x86-linux"
IUSE="debug"
