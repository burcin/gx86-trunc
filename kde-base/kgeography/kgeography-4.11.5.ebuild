# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /mnt/disk/cgit/cvsroot/gentoo-x86/kde-base/kgeography/kgeography-4.11.5.ebuild,v 1.6 2014/02/23 09:08:40 ago Exp $

EAPI=5

KDE_HANDBOOK="optional"
inherit kde4-base

DESCRIPTION="KDE: a geography learning tool"
HOMEPAGE="http://edu.kde.org/applications/miscellaneous/kgeography
http://edu.kde.org/applications/all/kgeography"
KEYWORDS="amd64 ~arm ppc ppc64 x86 ~amd64-linux ~x86-linux"
IUSE="debug"
