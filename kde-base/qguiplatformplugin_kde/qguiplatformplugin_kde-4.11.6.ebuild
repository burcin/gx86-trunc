# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /mnt/disk/cgit/cvsroot/gentoo-x86/kde-base/qguiplatformplugin_kde/qguiplatformplugin_kde-4.11.6.ebuild,v 1.1 2014/02/06 23:20:27 dilfridge Exp $

EAPI=5

KMNAME="kde-workspace"
inherit kde4-meta

DESCRIPTION="Helps integration of pure Qt applications with KDE Workspace"
KEYWORDS=" ~amd64 ~arm ~ppc ~ppc64 ~x86 ~amd64-linux ~x86-linux"
IUSE="debug"
